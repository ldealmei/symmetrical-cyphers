/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package symciphers.util;

import symciphers.ciphers.Cesar;
import symciphers.util.Util;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is a main class of Frequence analysis 
 * @author Lucas
 */
public class Analyse extends Util {

    private HashMap<Character, Integer> counter;
    private HashMap<Character, Double> frequency;
    private int soma;

    public Analyse() {
        super();
        counter = new HashMap<>();
        frequency = new HashMap<>();
    }

    /**
     * Read some text and get their letter frequency 
     * @param path
     * @throws IOException 
     */
    public void readText(String path) throws IOException {

        ArrayList<String> txt = readFile(path);
        /*
         Count each letter on the text
         */
        String msg;
        int cont = 0;
        for (int x = 0; x < txt.size(); x++) {
            msg = txt.get(x);
            for (int i = 0; i < msg.length(); i++) {
                if (msg.charAt(i) == ' ' || msg.charAt(i) == '.' || msg.charAt(i) == ',' || msg.charAt(i) == ';' || msg.charAt(i) == ':' || msg.charAt(i) == '!' || msg.charAt(i) == '?') {

                } else {
                    if (counter.containsKey(msg.charAt(i))) {
                        cont = counter.get(msg.charAt(i));
                    }
                    counter.put(msg.charAt(i), cont + 1);
                    soma++;
                }
                cont = 0;
            }
        }
        //System.out.println("soma: " + soma);
        /*
         Calculate the frequency of each letter
         */
        double freq;
        for (Entry<Character, Integer> mp : counter.entrySet()) {
            //System.out.println(mp.getKey().toString() + ":" + mp.getValue());
            freq = (double) mp.getValue() / soma;
            frequency.put(mp.getKey(), freq * 100);
            //System.out.println(freq * 100);
        }
    }

    /**
     * Initialize the process of frequency analysis
     * @param s 
     */
    public void initialize(String s) {
        try {
            String cmd[] = s.split(" ");
            readText(cmd[1]);
            
            frequency = (HashMap<Character, Double>) sortMap(frequency);
            System.out.println("\n---Letter Frequency---\n");
            /*
            Just compare with the most frequent letter on the text
            */
            String a = null,b = null;
            for (Entry<Character, Double> mp : frequency.entrySet()) {
                System.out.println(mp.getKey().toString() + ":" + mp.getValue());
                a = mp.getKey().toString();
                if(cmd.length == 3){
                    break;
                }
            }

            if(cmd.length == 3){
                HashMap<Character, Double> freqComum = (HashMap<Character, Double>) upCommonFrequency(cmd[2]);
                System.out.println("\n---Frequency Table---\n");
                for (Entry<Character, Double> mp : freqComum.entrySet()) {
                    //System.out.println(mp.getKey().toString() + ":" + mp.getValue());
                    b = mp.getKey().toString();
                    break;
                }
                Cesar cesar = new Cesar();
                int[] res = cesar.compareLetters(a.trim().charAt(0), b.trim().charAt(0));
                System.out.println("k': "+res[0]+"\nk'': "+res[1]);
            }
            
        } catch (IOException ex) {
            Logger.getLogger(Analyse.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
