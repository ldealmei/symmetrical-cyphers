/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package symciphers.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * This is a general class to help the other ciphers and the frequency analysis
 *
 * @author Lucas
 */
public class Util {
    
    public HashMap<Integer, Character> dicByID;
    public HashMap<Character, Integer> dicByLetter;
    private String dictionary = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    
    public Util(){
        dicByID = new HashMap<>();
        dicByLetter = new HashMap<>();
        for (int i = 0; i < dictionary.length(); i++) {
            dicByID.put(i, dictionary.charAt(i));
            dicByLetter.put(dictionary.charAt(i), i);
        }
    }

    /**
     * This function read a file and return a ArrayList of Strings to help and
     * the other parts of the program
     *
     * @param path
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public ArrayList<String> readFile(String path) throws FileNotFoundException, IOException {
        ArrayList<String> msg = new ArrayList<>();
        BufferedReader buffRead = new BufferedReader(new FileReader(path));
        String linha = "";
        while (true) {
            linha = buffRead.readLine();
            if (linha != null) {
                System.out.println(linha);
                msg.add(removeMarkets(linha));
            } else {
                break;
            }
        }
        buffRead.close();
        return msg;
    }

    /**
     * This function write a file that was generated in the system
     *
     * @param msg
     * @param path
     * @throws IOException
     */
    public void writeFile(ArrayList<String> msg, String path) throws IOException {
        BufferedWriter buffWrite = new BufferedWriter(new FileWriter(path));
        for (int i = 0; i < msg.size(); i++) {
            buffWrite.append(msg.get(i) + "\n");
        }
        buffWrite.close();
    }

    /**
     * This function help to up the common frequency of letters in some language
     *
     * @param path
     * @return
     * @throws IOException
     */
    public Map<Character, Double> upCommonFrequency(String path) throws IOException {
        Map<Character, Double> freq = new HashMap<>();
        ArrayList<String> txt = readFile(path);
        String str[];
        for (String linha : txt) {
            str = linha.split(":");
            freq.put(str[0].charAt(0), Double.parseDouble(str[1]));
        }
        return sortMap(freq);
    }

    /**
     * This function help to sort some HashMaps on the system
     *
     * @param unsortMap
     * @return
     */
    public Map<Character, Double> sortMap(Map<Character, Double> unsortMap) {
        List list = new LinkedList(unsortMap.entrySet());

        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o2)).getValue())
                        .compareTo(((Map.Entry) (o1)).getValue());
            }
        });

        Map sortedMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

    /**
     * This function helps to remove the markets from the text
     *
     * @param msg
     * @return
     */
    public String removeMarkets(String msg) {
        String normal = Normalizer.normalize(msg, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(normal).replaceAll("");
    }

    /**
     * This function is to compare two letters inside of the dictionary
     * @param a
     * @param b
     * @return 
     */
    public int[] compareLetters(char a, char b) {
        //System.out.println("a = "+a+"\nb = "+b);
        int[] resultado = new int[2];
        
        int numA = dicByLetter.get(a); 
        int numB = dicByLetter.get(b);
        String str = ""+b;
        int numb = dicByLetter.get(str.toLowerCase().charAt(0));
        
        /*
        Faz a comparação para os dois casos, com a letra maiuscula e minuscula
        */
        resultado[0] = compareNumbers(numA, numB);
        resultado[1] = compareNumbers(numA, numb);        
        
        return resultado;
    }
    
    /**
     * This function is to compare numbers 
     * @param x
     * @param y
     * @return 
     */
    public int compareNumbers(int x, int y){
        //System.out.println("x = "+x+"\ny = "+y);
        if(x > y){
            return x-y;
        } else {
            return y-x;
        }
    }
}
