/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package symciphers;

import java.util.Scanner;
import symciphers.util.Analyse;
import symciphers.ciphers.Cesar;
import symciphers.ciphers.Vernam;

/**
 *
 * @author Lucas
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("*** Simetric Ciphers ***\n"
                    + "\nCommands:"
                    + "\nObs: "
                    + "\n   -That 'n' is a numer higher than 0"
                    + "\n   -You can define the txt/dat name, just remember that for input/read you need the correct name"
                    + "\n   - [] ->  means that is optional\n"
                    + "\nCesar's Cipher:"
                    + "\n encrypt:  cesar -c -k n < inputText.txt > encrypted.dat"
                    + "\n decrypt:  cesar -d -k n < encrypted.dat > outputDecrypted.txt\n"
                    + "\nFrequency Analysis:"
                    + "\n anlysis: analyse encrypted.dat [frequency.txt]\n"
                    + "\nVernam's Cipher:"
                    + "\n encrypt: vernam -c key.dat < inputText.txt > encrypted.dat //the key will be create, you can decide the name"
                    + "\n decrypt: vernam -d key.dat < encrypted.dat > outputDecrypted.txt\n"
                    + "\nexit"
                    + "\n\nEnter with your command:");
            String command = scanner.nextLine();
            String cmd[] = command.split(" ");

            if (cmd[0].equalsIgnoreCase("cesar")) {
                Cesar cesar = new Cesar();
                cesar.initialize(command);
            } else if (cmd[0].equalsIgnoreCase("analyse")) {
                Analyse analise = new Analyse();
                analise.initialize(command);
            } else if (cmd[0].equalsIgnoreCase("vernam")) {
                Vernam vernam = new Vernam();
                vernam.initialize(command);
//                String msg = "Escrever um programa cifrador de Vernam que funcione da seguinte forma:";
//                String k = vernam.getKey(msg);
//                String cif = vernam.cifrar(msg, k);
//                String dec = vernam.decifrar(cif, k);
//                System.out.println("msg: " + msg);
//                System.out.println("\nkey: " + k);
//                System.out.println("\ncif: " + cif);
//                System.out.println("\ndec: " + dec);
            } else if (cmd[0].equalsIgnoreCase("exit")) {
                break;
            } else {
                System.out.println("Comando inválido!");
            }
            System.out.println("\n--------------------------\n");
        }

    }

}
