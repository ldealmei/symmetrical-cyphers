/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package symciphers.ciphers;

import symciphers.util.Util;
import symciphers.Main;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lucas
 */
public class Cesar extends Util {

    private ArrayList<String> msgCrypted;
    private ArrayList<String> msgOpen;

    public Cesar() {
        super();
    }

    /**
     * This function allows the system to encryp some String from the Cesar
     * Cipher
     *
     * @param msg
     * @param k
     * @return
     */
    public String encrypt(String msg, int k) {
        int n;
        String cif = "";
        for (int i = 0; i < msg.length(); i++) {
            if (msg.charAt(i) == ' ' || msg.charAt(i) == '.' || msg.charAt(i) == ',' || msg.charAt(i) == ';' || msg.charAt(i) == ':' || msg.charAt(i) == '!' || msg.charAt(i) == '?') {
                cif += msg.charAt(i);
                //System.out.print(" ");
            } else {
                n = dicByLetter.get(msg.charAt(i)) + k;
                if (n > dicByLetter.size()) {
                    n = n - dicByLetter.size();
                }
                cif += dicByID.get(n);
                //System.out.print(dic1.get(n));
            }
        }
        return cif;
    }

    /**
     * This function allows the system to decrypt from the Cesars cipher
     *
     * @param msg
     * @param k
     * @return
     */
    public String decrypt(String msg, int k) {
        int n;
        String dec = "";
        for (int i = 0; i < msg.length(); i++) {
            if (msg.charAt(i) == ' ' || msg.charAt(i) == '.' || msg.charAt(i) == ',' || msg.charAt(i) == ';' || msg.charAt(i) == ':' || msg.charAt(i) == '!' || msg.charAt(i) == '?') {
                dec += msg.charAt(i);
                //System.out.print(" ");
            } else {
                n = dicByLetter.get(msg.charAt(i)) - k;
                if (n < 0) {
                    n = dicByLetter.size() + n;
                }
                dec += dicByID.get(n);
                //System.out.print(dic1.get(n));
            }
        }
        return dec;
    }

    /**
     * This functions initialize the process of use the Cesar`s cipher
     *
     * @param s
     */
    public void initialize(String s) {
        msgOpen = new ArrayList<>();;
        msgCrypted = new ArrayList<>();
        String msg;

        String cmd[] = s.split(" ");

        Util arq = new Util();
        int k = Integer.parseInt(cmd[3]);

        if (cmd[1].equalsIgnoreCase("-c")) {
            try {
                msgOpen = arq.readFile(cmd[5]);
                for (int i = 0; i < msgOpen.size(); i++) {
                    msg = encrypt(msgOpen.get(i), k);
                    System.out.println("Msg: " + msg);
                    msgCrypted.add(msg);
                }
                arq.writeFile(msgCrypted, cmd[7]);

            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (cmd[1].equalsIgnoreCase("-d")) {
            try {
                msgCrypted = arq.readFile(cmd[5]);
                for (int i = 0; i < msgCrypted.size(); i++) {
                    msg = decrypt(msgCrypted.get(i), k);
                    System.out.println("Msg: " + msg);
                    msgOpen.add(msg);
                }
                arq.writeFile(msgOpen, cmd[7]);

            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

}
