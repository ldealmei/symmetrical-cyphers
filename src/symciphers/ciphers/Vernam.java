/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package symciphers.ciphers;

import symciphers.util.Util;
import symciphers.Main;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lucas
 */
public class Vernam extends Util {

    private ArrayList<String> msgCrypted;
    private ArrayList<String> msgOpen;
    private ArrayList<String> key;

    public Vernam() {
        super();
    }

    /**
     * This function initialize the Vernam Cipher process
     *
     * @param s
     */
    public void initialize(String s) {
        msgOpen = new ArrayList<>();;
        msgCrypted = new ArrayList<>();
        String msg;

        String cmd[] = s.split(" ");

        Util arq = new Util();

        if (cmd[1].equalsIgnoreCase("-c")) {
            try {
                msgOpen = arq.readFile(cmd[4]);
                key = new ArrayList<>();
                for (int i = 0; i < msgOpen.size(); i++) {
                    String k = generateKey(msgOpen.get(i));
                    msg = encrypt(msgOpen.get(i), k);
                    System.out.println("Msg: " + msg);
                    msgCrypted.add(msg);
                    key.add(k);
                }
                arq.writeFile(msgCrypted, cmd[6]);
                arq.writeFile(key, cmd[2]);

            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (cmd[1].equalsIgnoreCase("-d")) {
            try {
                key = arq.readFile(cmd[2]);
                msgCrypted = arq.readFile(cmd[4]);
                for (int i = 0; i < msgCrypted.size(); i++) {
                    msg = decrypt(msgCrypted.get(i), key.get(i));
                    System.out.println("Msg: " + msg);
                    msgOpen.add(msg);
                }
                arq.writeFile(msgOpen, cmd[6]);

            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    /**
     * This function creates new key for a text from vernams cipher
     *
     * @param msg
     * @return
     */
    public String generateKey(String msg) {
        int size = msg.length();
        Random rd = new Random();
        String k = "";
        for (int i = 0; i < size; i++) {
            int a = rd.nextInt(dicByLetter.size()) + 1;
            if (msg.charAt(i) == ' ' || msg.charAt(i) == '.' || msg.charAt(i) == ',' || msg.charAt(i) == ';' || msg.charAt(i) == ':' || msg.charAt(i) == '!' || msg.charAt(i) == '?') {
                k += msg.charAt(i);
            } else {
                while (true) {
                    if ((a + dicByLetter.get(msg.charAt(i))) > dicByLetter.size() || dicByID.get(a) == null) {
                        a = dicByLetter.size() - (rd.nextInt(dicByLetter.size()) + 1);
                    } else {
                        break;
                    }
                }
                k += dicByID.get(a);
            }

        }
        return k;
    }

    /**
     * Function to encrypt a message
     *
     * @param msg
     * @param key
     * @return
     */
    public String encrypt(String msg, String key) {
        System.out.println(key);
        int a, b, c;
        String cif = "";
        for (int i = 0; i < msg.length(); i++) {
            if (msg.charAt(i) == ' ' || msg.charAt(i) == '.' || msg.charAt(i) == ',' || msg.charAt(i) == ';' || msg.charAt(i) == ':' || msg.charAt(i) == '!' || msg.charAt(i) == '?') {
                cif += msg.charAt(i);
            } else {
                a = dicByLetter.get(msg.charAt(i));
                b = dicByLetter.get(key.charAt(i));
                c = a + b;
                if (c > dicByLetter.size()) {
                    c = c - dicByLetter.size();
                }
                cif += dicByID.get(c);
            }
        }

        return cif;
    }

    /**
     * Function to decrypt a message from Vernams Cipher
     *
     * @param msg
     * @param key
     * @return
     */
    public String decrypt(String msg, String key) {
        int a, b, c;
        String dec = "";
        for (int i = 0; i < msg.length(); i++) {
            if (msg.charAt(i) == ' ' || msg.charAt(i) == '.' || msg.charAt(i) == ',' || msg.charAt(i) == ';' || msg.charAt(i) == ':' || msg.charAt(i) == '!' || msg.charAt(i) == '?') {
                dec += msg.charAt(i);
            } else {
                a = dicByLetter.get(msg.charAt(i));
                b = dicByLetter.get(key.charAt(i));
                c = a - b;
                if (c < 0) {
                    c = dicByLetter.size() - c;
                }

                dec += dicByID.get(c);
            }
        }
        return dec;
    }

    public int getNumber(char c) {
        return dicByLetter.get(c);
    }

}
