# Implementação de cifradores simétricos #

O objetivo deste trabalho é compreender melhor o funcionamento dos algoritmos de cifragem simétrica mais simples.

Todo Aluno deve implementar e defender:

* Um cifrador de César.
*     Um analisador de frequencia.
*         usar este no texto criptografado abaixo junto com a análise de frequências abaixo e decodificar corretamente a mensagem proposta.
*     Um cifrador de Vernam.
*         usar este no texto aberto encontrado junto com a análise de frequências.
*         O algoritmo de Vernam é vulnerável à análise de frequências?
*     Usar o cifrador RC4, para conhecer o funcionamento.
*         usar este no texto aberto encontrado junto com a análise de frequências.
*         O algoritmo RC4 é vulnerável à análise de frequências?

## O cifrador de César ##

Produzir uma implementação do cifrador de César. Ele deve funcionar da seguinte forma:

Para cifrar:

    cesar -c -k 5 < texto-aberto.txt > texto-cifrado.txt

Para decifrar:

    cesar -d -k 5 < texto-cifrado.txt > texto-aberto.txt

Opções:

    -c : cifrar
    -d : decifrar
    -k n : valor da chave a ser usada

A rotação de caracteres deve ser feita somente sobre os caracteres [A-Z,a-z,0-9]. Caracteres acentuados devem ser tratados sem acento.

## Análise de frequências ##

A análise de frequências é uma técnica simples de criptanálise que consiste em identificar os caracteres do texto cifrado usando a frequência de uso dos caracteres na língua em que se supõe que a mensagem esteja escrita. O algoritmo de César é um cifrador de substituição simples e portanto vulnerável a essa técnica.

Considerando a tabela de frequências de caracteres em português informada abaixo ([obtida deste site](http://www.numaboa.com.br/criptografia/criptoanalise/310-Frequencia-no-Portugues)), escreva um programa para fazer a criptanálise de uma mensagem cifrada com o cifrador de César.

A mensagem a ser analisada é:

    g5Bt5 t54yvtz3v4A5 wrG t53 7Bv r9 6v995r9 9v 9z4Ar3
    58xB2y59r9. dBzA5 t54yvtz3v4A5, 7Bv 9v 9z4Ar3
    yB3z2uv9. Vy r99z3 7Bv r9 v96zxr9 9v3 x8r59 v8xBv3
    uv9uv4y59r3v4Av r trsvtr 6r8r 5 tvB, v47Br4A5 r9
    tyvzr9 r9 srzEr3 6r8r r Av88r, 9Br 3rv.
    cv54r8u5 Ur mz4tz.

## O cifrador de Vernam ##

Escrever um programa cifrador de Vernam que funcione da seguinte forma:

vernam -c chave.dat < texto-aberto.txt > texto-cifrado.txt

Atividade: aplicar o cifrador de Vernam ao texto aberto da atividade anterior e efetuar a análise de frequências do arquivo de saída. Comparar a distribuição de frequências da entrada com a da saída.

Questões:

* Como será feita a geração da chave?
*     O algoritmo de Vernam é vulnerável à análise de frequências?